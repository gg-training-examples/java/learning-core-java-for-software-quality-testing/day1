package com.groundgurus.day1;

public class ForExample {
	public static void main(String[] args) {
		String days[] = { "Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun" };
//		for (String day : days) {
//			System.out.println(day);
//		}

		// from "Mon" to "Sun"
		for (int i = 0; i < days.length; i+=2) {
			System.out.println(days[i]);
		}
		
		// from "Sun" to "Mon"
		for (int i = days.length - 1; i >= 0; i--) {
			System.out.println(days[i]);
		}
		
	}
}
