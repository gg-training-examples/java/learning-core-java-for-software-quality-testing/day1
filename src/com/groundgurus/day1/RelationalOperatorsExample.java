package com.groundgurus.day1;

public class RelationalOperatorsExample {
	public static void main(String[] args) {
		int a = 30;
		double b = 40.123;
		
		System.out.println(a > b);  // is a greater than b?
		System.out.println(a == b); // is a equal to b?
		System.out.println(a != b); // is a not equal to b?
	}
}
