package com.groundgurus.day1;

import java.text.SimpleDateFormat;
import java.util.Date;

public class WhatIsTheDateToday { // title case
	public static void main(String[] args) {
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		String today = sdf.format(new Date());
		System.out.println(today);
		
		String theBigRedFoxJumpsOverTheLazyDog; // camel case
	}
}
