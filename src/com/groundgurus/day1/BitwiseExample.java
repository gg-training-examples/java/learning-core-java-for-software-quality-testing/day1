package com.groundgurus.day1;

public class BitwiseExample {
	public static void main(String[] args) {
		int x = 20 & 14;
		//   0001 0100
		// & 0000 1110
		// -----------
		//   0000 0100
		System.out.println(x);
		
		int z = 20 | 14;
	    //   0001 0100
		// | 0000 1110
		// -----------
		//   0001 1110 = 16 + 8 + 4 + 2
		System.out.println(z);
		
		int y = 20 ^ 14;
	    //   0001 0100
		// ^ 0000 1110
		// -----------
		//   0001 1010 = 16 + 8 + 2
		System.out.println(y);
	}
}
