package com.groundgurus.day1;

public class IfExample {
	public static void main(String[] args) {
		int score = 89;
		
		if (score >= 90) {
			System.out.println("Success");
			System.out.println("You passed the exam");
		} else { // executed when the if condition returns "false"
			System.out.println("You have failed this city!");
		}
	
	}
}
