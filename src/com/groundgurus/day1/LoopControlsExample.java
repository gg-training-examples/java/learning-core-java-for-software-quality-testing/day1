package com.groundgurus.day1;

public class LoopControlsExample {
	public static void main(String[] args) {
		hoopla:
		for (int i = 1; i <= 10; i++) {
			for (int j = 1; j <= 5; j++) {
				if (i == 2 && j == 2) {
					break hoopla;
				} else {
					System.out.println("i = " + i + ", j = " + j);
				}
			}
		}
	}
}
