package com.groundgurus.day1.exercises;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Exercise2 {
	public static void main(String[] args) {
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss");
		String today = sdf.format(new Date());
		System.out.println(today);
	}
}
