package com.groundgurus.day1.exercises;

import java.util.Scanner;

public class Exercise4 {
	public static void main(String[] args) {
		int[] numbers = { 5, 6, 4, 12, 10 };
		
		Scanner scanner = new Scanner(System.in);
		
		System.out.print("Enter a value: ");
		int value = scanner.nextInt();
		int index = -1;
		
		for (int i = 0; i < numbers.length; i++) {
			if (value == numbers[i]) {
				index = i;
				break;
			}
		}
		
		if (index != -1) {
			System.out.println(value + " is in the index " + index);
		} else {
			System.out.println(value + " not found. index " + index);
		}
		
		scanner.close();
	}
}
