package com.groundgurus.day1.exercises;

import java.util.Scanner;

public class Exercise3 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		
		System.out.print("Enter first number:");
		double firstNumber = scanner.nextDouble();
		
		System.out.print("Enter second number:");
		double secondNumber = scanner.nextDouble();
		
		if (firstNumber > secondNumber) {
			System.out.println(firstNumber + " is greather than " + secondNumber);
		} else {
			System.out.println(secondNumber + " is greather than " + firstNumber);
		}
		
		scanner.close();
	}
}
