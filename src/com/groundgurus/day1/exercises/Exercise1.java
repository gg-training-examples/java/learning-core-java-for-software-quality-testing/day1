package com.groundgurus.day1.exercises;

import java.util.Scanner;

public class Exercise1 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		
		System.out.print("Enter first number:");
		double firstNumber = scanner.nextDouble();
		
		System.out.print("Enter second number:");
		double secondNumber = scanner.nextDouble();
		
		double sum = firstNumber + secondNumber;
		System.out.println("The sum is " + sum);

		double difference = firstNumber - secondNumber;
		System.out.println("The difference is " + difference);
		
		double product = firstNumber * secondNumber;
		System.out.println("The product is " + product);
		
		double quotient = firstNumber / secondNumber;
		System.out.println("The quotient is " + quotient);
		
		scanner.close();
	}
}
