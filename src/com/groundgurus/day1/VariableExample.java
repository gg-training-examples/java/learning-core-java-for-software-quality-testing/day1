package com.groundgurus.day1;

import java.math.BigDecimal;

public class VariableExample {
	public static void main(String[] args) {
		int score = 32; // from -2^31 to 2^31 - 1
		int b = 64; // from -2^7 to 2^7 - 1, -128 to 127
	
		b = 129;
		System.out.println(b);
		
		double price = 29.99;
		BigDecimal money = new BigDecimal(2000.0); // 128 bits
		
		boolean hasEntered = false;
		
		char department = '\u0197';
		System.out.println(department);
	}
}
