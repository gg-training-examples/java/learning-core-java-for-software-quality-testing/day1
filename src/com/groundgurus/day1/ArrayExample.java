package com.groundgurus.day1;

public class ArrayExample {
	public static void main(String[] args) {
		//int stuff[] = { 1, 2, 3 }; // shortcut
		int stuff[] = new int[] { 1, 2, 3, 0, 0 };

		String fruits[] = new String[5];
		fruits[0] = "apple";
		fruits[1] = "banana";
		fruits[2] = "corn";
		
		System.out.println(stuff[0]);
	}
}
