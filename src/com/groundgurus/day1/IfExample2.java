package com.groundgurus.day1;

public class IfExample2 {
	public static void main(String[] args) {
		int score = 91;
		
		if (score == 100) {
		    System.out.println("Fantastic!");
		} else if (score >= 90 && score <= 99) {
		    System.out.println("Geronimo!");
		} else if (score >= 80 && score <= 89) {
		    System.out.println("Allons-y!");
		} else if (score >= 75 && score <= 79) {
		    System.out.println("Meh!");
		} else {
		    System.out.println("Doh!");
		}
	}
}
