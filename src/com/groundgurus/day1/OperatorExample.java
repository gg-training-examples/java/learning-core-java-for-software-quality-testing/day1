package com.groundgurus.day1;

public class OperatorExample {
	public static void main(String[] args) {
		int x = 0;   // 1
		int y = ++x; // 1
		
		System.out.println("x = " + x);
		System.out.println("y = " + y);
		
		// 0000 0001 ~ 
		// 1111 1110
		
		int z = ~y;
		
		System.out.println("z = " + z);
		
		int a = 1;
		a += 2; // a = a + 2
		System.out.println(a);
		
		
		
		
		int total = 10 + 20 + 30 + 40 + 50;

		String resultMessage = "You have a total of " + (10 + 20 * 30 - 40 / 50);
		                   //                           (10 + 600 - 40 / 50)
		                   //                           (10 + 600 - 0)
		System.out.println(resultMessage);
		
		double salary = 90_000_000;
	}
}
