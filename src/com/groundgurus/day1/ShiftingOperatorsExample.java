package com.groundgurus.day1;

public class ShiftingOperatorsExample {
	public static void main(String[] args) {
		int x = 20 << 2; // Left shift
		//     0001 0100 = 20
		// <<  0101 0000 = 16 + 64 = 80
		System.out.println(x);
		
		int y = 20 >> 2; // Right shift
		//	  0001 0100 = 20
		// >> 0000 0101 = 4 + 1 = 5
		System.out.println(y);
	}
}
