package com.groundgurus.day1;

import java.util.Scanner;

public class InputExample {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		
		System.out.print("Enter a number: ");
		int number = scanner.nextInt();
		
		System.out.println("You've entered " + number);
		
		scanner.close();
	}
}
